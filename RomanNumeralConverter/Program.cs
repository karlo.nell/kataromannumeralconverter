﻿using System;
using System.Collections.Generic;

namespace RomanNumeralConverter
{
    public class Program
    {
        public static Dictionary<char, int> RomanDictionary = new Dictionary<char, int>()
        {
            {'I', 1 },
            {'V', 5 },
            {'X', 10 },
            {'L', 50 },
            {'C', 100 },
            {'D', 500 },
            {'M', 1000 }
        };
        public static void Main(string[] args)
        {
            RomanNumeralConverter("XIX");
        }
        public static int RomanNumeralConverter(string romanNumerals)
        {
            int total = 0;
            int[] numericalValues = new int[romanNumerals.Length];

            #region Convert romans to their numerical equivalent
            //Convert all roman values to numbers in an int array
            for (int i = 0; i < romanNumerals.Length; i++)
            {
                numericalValues[i] = RomanDictionary[romanNumerals[i]];
            }
            #endregion

            //Basically calculate on integers instead of roman numerals
            //change the negative occurences in the int array
            for (int i = 0; i < numericalValues.Length - 1; i++)
            {
                //checks if next value is "larger"
                if (numericalValues[i] < numericalValues[i + 1])
                {
                    //turn value negative of the left value e.g. (1, 10 = -1+10)
                    numericalValues[i] = numericalValues[i] * -1;
                }
                else continue; // Do nothing and go to next value
            }

            #region Calculate total
            //calculate total
            foreach (var number in numericalValues)
            {
                total += number;
            }
            #endregion

            return total;
        }
    }
}
